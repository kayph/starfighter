!define VERSION "1.4"

Name "starfighter ${VERSION}"
OutFile "starfighter-${VERSION}.exe"
SetCompressor lzma

Page components
Page directory
Page license
Page instfiles

InstallDir $PROGRAMFILES\starfighter
Icon "starfighter.ico"
LicenseData "..\COPYING"

RequestExecutionLevel admin
!include LogicLib.nsh

Function .onInit
    UserInfo::GetAccountType
    pop $0
    ${if} $0 != "admin"
        MessageBox mb_iconstop "Administrator rights required!"
        SetErrorLevel 740 ; ERROR_ELEVATION_REQUIRED
        Quit
    ${endif}
FunctionEnd

Section "Starfighter"
    SetOutPath "$INSTDIR\music\"
    File ..\music\*
    SetOutPath "$INSTDIR\sound\"
    File ..\sound\*
    SetOutPath "$INSTDIR\gfx\"
    File ..\gfx\*
    SetOutPath "$INSTDIR\data\"
    File ..\data\*
    SetOutPath $INSTDIR
    File ..\COPYING
    File ..\LICENSES
    File ..\README.txt
    WriteUninstaller "$INSTDIR\uninstall.exe"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Starfighter" "DisplayName" "Starfighter"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Starfighter" "UninstallString" "$INSTDIR\uninstall.exe"

    File ..\build\src\starfighter.exe
    File ..\build\libFLAC-8.dll
    File ..\build\libjpeg-9.dll
    File ..\build\libmodplug-1.dll
    File ..\build\libogg-0.dll
    File ..\build\libpng16-16.dll
    File ..\build\libtiff-5.dll
    File ..\build\libwebp-4.dll
    File ..\build\libvorbis-0.dll
    File ..\build\libvorbisfile-3.dll
    File ..\build\SDL2.dll
    File ..\build\SDL2_image.dll
    File ..\build\SDL2_mixer.dll
    File ..\build\smpeg2.dll
    File ..\build\zlib1.dll
SectionEnd

Section "Start desktop shortcut"
    SetOutPath $INSTDIR
    CreateShortCut "$DESKTOP\starfighter.lnk" "$INSTDIR\starfighter.exe" "" "$INSTDIR\starfighter.exe" 0
SectionEnd

Section "Start menu shortcut"
    CreateShortCut "$SMPROGRAMS\Starfighter\starfighter.lnk" "$INSTDIR\starfighter.exe" "" "$INSTDIR\starfighter.exe" 0
SectionEnd

Section "Uninstall"
    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Starfighter"
    Delete "$DESKTOP\starfighter.lnk"
    RMDir /r "$SMPROGRAMS\starfighter"
    RMDir /r $INSTDIR
    Delete $INSTDIR\uninstall.exe
    RMDir $INSTDIR
SectionEnd

