#include <stdbool.h>
#include <windows.h>
#include <shlobj.h>

#include "filesystem.h"

char *get_home_directory(char *path)
{
    if (SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path) != S_OK)
        return NULL;

    return path;
}

bool create_directory(const char *path)
{
    if (CreateDirectory(path, NULL) || GetLastError() == ERROR_ALREADY_EXISTS)
        return true;

    return false;
}

