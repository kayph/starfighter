#ifndef __STARFIGHTER_FILESYSTEM_H__
#define __STARFIGHTER_FILESYSTEM_H__

#include <stdbool.h>

char *get_home_directory(char *path);
bool create_directory(const char *path);

#endif

