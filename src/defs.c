const char * const systemNames[] = {"Spirit", "Eyananth", "Mordor", "Sol"};

const char * const systemBackground[] = {
	"gfx/spirit.jpg", "gfx/eyananth.jpg", "gfx/mordor.jpg", "gfx/sol.jpg"
};

const int rate2reload[6] = {15, 15, 13, 11, 9, 7};
const int screenWidth = 800;
const int screenHeight = 600;
const int xViewBorder = 100;
const int yViewBorder = 100;
const float cameraMaxSpeed = 3.;
const int maxHoming = 20;
const int maxDoubleHoming = 15;
const int maxMicroHoming = 10;
