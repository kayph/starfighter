#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <string.h>

char *get_home_directory(char *path)
{
    const char *home = getenv("HOME");

    if (!home)
        return NULL;

    strcpy(path, home);

    return path;
}

bool create_directory(const char *path)
{
    if (mkdir(path, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH) != 0 && errno != EEXIST)
        return false;

    return true;
}

