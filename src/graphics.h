/*
Copyright (C) 2003 Parallel Realities
Copyright (C) 2011 Guus Sliepen
Copyright (C) 2015 Julian Marchant

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL.h>
#include <stdbool.h>

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Texture *texture;
extern SDL_Surface *screen, *background;
extern SDL_Surface *shape[MAX_SHAPES];
extern SDL_Surface *shipShape[MAX_SHIPSHAPES];
extern SDL_Surface *fontShape[MAX_FONTSHAPES];
extern SDL_Surface *shopSurface[MAX_SHOPSHAPES];
extern bRect *bufferHead;
extern bRect *bufferTail;
extern textObject textShape[MAX_TEXTSHAPES];
extern SDL_Surface *messageBox;


bool overlap_rect(float x0, float y0, int w0, int h0, float x2, float y2, int w1, int h1);
bool object_collides(object *object1, object *object2);
bool collectable_collide_object(collectables *object1, object *object2);

void initGraphics();
SDL_Surface *setTransparent(SDL_Surface *sprite);
void addBuffer(int x, int y, int w, int h);
void blit_image_target(SDL_Surface *image, int x, int y, SDL_Surface *dest);
void blit_image(SDL_Surface *image, int x, int y);
void blitText(int i);
void flushBuffer();
void unBuffer();
int draw_string_wrapped(const char *in, int x, int y, int fontColor, signed char wrap, SDL_Surface *dest);
int draw_string_target(const char *in, int x, int y, int fontColor, SDL_Surface *dest);
int draw_string(const char *in, int x, int y, int fontColor);
void drawBackGround();
void clearScreen(Uint32 color);
void updateScreen();
void delayFrame();
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void draw_line_target(SDL_Surface *dest, int x1, int y1, int x2, int y2, int col);
void draw_line(int x1, int y1, int x2, int y2, int col);
void circle(int xc, int yc, int R, SDL_Surface *PIX, int col);
void draw_rect_target(SDL_Surface *dest, int x, int y, int w, int h, Uint8 red, Uint8 green, Uint8 blue);
void draw_rect(int x, int y, int w, int h, Uint8 red, Uint8 green, Uint8 blue);
SDL_Surface *createSurface(int width, int height);
SDL_Surface *create_text_with_color(const char *inString, int color);
void textSurface(int index, const char *inString, int x, int y, int fontColor);
SDL_Surface *alphaRect(int width, int height, Uint8 red, Uint8 green, Uint8 blue);
void createMessageBox(SDL_Surface *face, const char *message, signed char transparent);
void freeGraphics();

SDL_Surface *loadImage(const char *filename);

#endif
